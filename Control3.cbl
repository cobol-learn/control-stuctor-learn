       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL3.
       AUTHOR. PAKAWAT.


       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  CITY-CODE   PIC   9.
           88 CITY-IS-DUBIN           VALUE 1.
           88 CITY-IS-LIMERICK        VALUE 2.
           88 CITY-IS-CORK            VALUE 3.
           88 CITY-IS-GALWAY          VALUE 4.
           88 CITY-IS-SLIGO           VALUE 5.
           88 CITY-IS-WATERFORD       VALUE 6.
           88 UNIVERSITY-CITY         VALUE 1  THROUGH 4.
           88 CITY-CODE-NOT-VALID     VALUE 0,7,8,9.

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "ENTER A CITY CODE (1-6) -" WITH NO ADVANCING 
           ACCEPT CITY-CODE 
           
           IF CITY-CODE-NOT-VALID THEN
              DISPLAY "invalid entered !!!"
           ELSE  
              IF CITY-IS-LIMERICK THEN 
                 DISPLAY "Hey we,re home"
              END-IF
              IF CITY-IS-DUBIN THEN
                 DISPLAY "Hey, we're in the capital"
              END-IF 
              IF UNIVERSITY-CITY THEN 
                 DISPLAY "Apply the rent surchrage!"   
              END-IF
           END-IF

           SET CITY-CODE-NOT-VALID  TO TRUE
           DISPLAY CITY-CODE 
           
           GOBACK 
           .
