       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL1.
       AUTHOR. BEST.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CLASS HEX-NUMBER  IS "0" THROUGH "9"  ,"A" THROUGH "F"
           CLASS REAL-NAME   IS "A"   THROUGH "Z" ,  'a'   THROUGH  "Z",
                 "'",SPACE.
       

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 NUM-IN PIC   X(4).
       01 NAME-IN   PIC   X(15).


       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "ENTER HEX A NUMBER - " WITH NO ADVANCING .
           ACCEPT NUM-IN .
           IF NUM-IN IS HEX-NUMBER  THEN
              DISPLAY NUM-IN " IS A HEX-NUMBER"
           ELSE
              DISPLAY NUM-IN " IS NOT A HEX-NUMBER"
           END-IF.
           
           
           DISPLAY "ENTER A NAME - " WITH NO ADVANCING .
           ACCEPT NAME-IN .
           IF NAME-IN  IS REAL-NAME  THEN
              DISPLAY NAME-IN " IS A HEX-NUMBER"
           ELSE
              DISPLAY NAME-IN " IS NOT A HEX-NUMBER"
           END-IF.
           

           