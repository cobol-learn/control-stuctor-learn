       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL5.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  VALIDATION-RETURN-CODE  PIC 9 VALUE 9.
           88 DATE-IS-OK  VALUE 0.
           88 DATE-IS-INVALID  VALUE 1 THROUGH 8.
           88 VALID-CODE-SUPPILED  VALUE 0 THROUGH 8.

       01  DATE-ERROR-MESSAGE   PIC   X(35) VALUE SPACE .
           88 DATE-NOT-NUMERIC  VALUE "Eror- The date must be numeric.".
           88 YEAR-IS-ZERO  VALUE "Eror- The year cannot be zero.".
           88 MONTH-IS-ZERO  VALUE "Eror- The month cannot be zero.".
           88 DAY-IS-ZERO  VALUE "Eror- The day cannot be zero.".
           88 YEAR-PASSED  VALUE "Eror- Year has already passed.".
           88 MOTH-TOO-BIG  VALUE "Eror- Month is greather than 12.".
           88 DAY-TOO-BIG  VALUE "Eror- Day is greather than 31.".
           88 TOO-BIG-FOR-MONTH  VALUE "Eror- Day to big for this month
      -    ".".

       PROCEDURE DIVISION .
       Begin.
           PERFORM VALIDATE-DATE UNTIL VALID-CODE-SUPPILED
           EVALUATE VALIDATION-RETURN-CODE 
              WHEN 0 SET  DATE-IS-OK TO TRUE 
              WHEN 1 SET  DATE-NOT-NUMERIC TO TRUE
              WHEN 2 SET  YEAR-IS-ZERO TO TRUE
              WHEN 3 SET  MONTH-IS-ZERO TO TRUE
              WHEN 4 SET  DAY-IS-ZERO TO TRUE 
              WHEN 5 SET  YEAR-PASSED TO TRUE
              WHEN 6 SET  MOTH-TOO-BIG TO TRUE
              WHEN 7 SET  DAY-TOO-BIG TO TRUE  
              WHEN 8 SET  TOO-BIG-FOR-MONTH TO TRUE 
           END-EVALUATE

           IF DATE-IS-INVALID 
              DISPLAY  DATE-ERROR-MESSAGE 
           END-IF 

           IF DATE-IS-OK 
              DISPLAY  "DATE IS OK."
           END-IF 
           GOBACK 
           .

       VALIDATE-DATE.
           DISPLAY "ENTER A VALIDATION RETURN CODE (0-8)" WITH NO 
           ADVANCING
           ACCEPT VALIDATION-RETURN-CODE 
           . 
           

           
